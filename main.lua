
require "libs/logger"
log = Logger:new()
Object = require "libs/classic"

function clearLoveCallbacks()
	love.draw = nil
	love.keypressed = nil
	love.update = nil
	love.load = nil
	love.mousepressed = nil
end

state = {}
function loadState(name)
	state = {}
	clearLoveCallbacks()
	local path = "src/states/" .. name
	require(path .. "/main")
	load()
end

function load()
end

function love.load()
	loadState("menu")
end