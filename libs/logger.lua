Logger = {}
Logger_mt = { __index = Logger}

function Logger:new()
  local self = {}
  setmetatable(self, Logger_mt)
  self.counter = 0
  self.log = io.open("debug/logged.txt", "w")
  return self
end

function Logger:add(message)
  self.log:write(self.counter .. " " .. message .. "\n")
  self.counter = self.counter + 1
end
