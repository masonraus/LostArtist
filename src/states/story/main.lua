local counter = 0

function load()
	flailed = love.graphics.newFont("assets/fonts/Flailed/Flailed.ttf")
	love.graphics.setBackgroundColor(237, 212, 145)
	
end

function love.draw()
	if counter == 0 then
		love.graphics.printf("You, lone artist, are lost and struggling to stay alive." .. 
			"You came here seeking the mystical eighth color but like so many others you" ..
			" have found yourself lost in the catacombs of The Corrupted Saint Paint. ",
			10, 10, 200, "left", 0, 4, 4)
	elseif counter == 1 then
		love.graphics.printf("You thought the curse would help, like the string in " ..
			"the labyrinth, but you were wrong. Paths crossing back and forth and no " ..
			"way to tell which came first. Worst of all, you can't seem to shake the " .. 
			"feeling that this is how the monsters keep finding you...",
			10, 10, 200, "left", 0, 4, 4)
	else
		loadState("game")
	end
	love.graphics.print("Press any key to Continue...", 500, 580, 0, 2, 2)
end

function love.keypressed(key,scancode,isrepeat)
	if key then
		counter = counter + 1
	end
end