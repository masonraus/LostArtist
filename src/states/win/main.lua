setmetatable(_G, { __newindex = function (k, v) error('global ' .. v, 2) end })
local ROT = require "libs/rotLove/rot"
setmetatable(_G, nil)

function load()
	frame = ROT.TextDisplay()
end

function love.draw()
	frame:draw()
end

x,y,i=1,1,64

function love.update()
    if x<80 then x=x+1
    else x,y=1,y<24 and y+1 or 1
    end
    i = i<120 and i+1 or 64
    frame:write(" ", x, y, getRandomColor(), getRandomColor())
		frame:writeCenter("You Win!", 1, getRandomColor(), {0, 0, 0, 255})
end

function getRandomColor()
    return { math.floor(ROT.RNG:random(0,255)),
             math.floor(ROT.RNG:random(0,255)),
             math.floor(ROT.RNG:random(0,255)),
             255}
end

function love.keypressed(key,scancode,isrepeat)
	if key == 'escape' then
		love.event.quit(0)
	end
end