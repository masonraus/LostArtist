
function load()
	title = love.graphics.newImage("assets/sprites/Title.png")
	flailed = love.graphics.newFont("assets/fonts/Flailed/Flailed.ttf")
	love.graphics.setColor(0, 0, 0)
end

function love.draw()
	love.graphics.setFont(flailed)
	love.graphics.setBackgroundColor(140, 0, 93)
	love.graphics.draw(title, 250, 50, 0, 1.5, 1.5)
	
	love.graphics.rectangle("line", 100, 350, 200, 100)
	love.graphics.print("New Game", 140, 375, 0, 2, 2)
	
	love.graphics.rectangle("line", 400, 350, 200, 100)
	love.graphics.print("Quit", 440, 375, 0, 2, 2)
end

function love.mousepressed(x,y,button,isTouch)
	if x > 400 and x < 600 and y > 350 and y < 450 then
		love.event.quit(0)
	elseif x > 100 and x < 300 and y > 350 and y < 450 then
		loadState("story")
	end
end