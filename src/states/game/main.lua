setmetatable(_G, { __newindex = function (k, v) error('global ' .. v, 2) end })
local ROT = require "libs/rotLove/rot"
setmetatable(_G, nil)

function load()
	f = ROT.Display(80, 24)
	map = {}
	field = {}
	seen = {}
	traveled = {}
	seenColor = {100, 100, 100, 255}
	fieldColor = {255, 255, 255, 255}
	fieldBg = {50, 50, 50, 255}
	traveledBg = {179, 98, 173, 255}
	update = false
	player = {x = 1, y = 1, wep = {name = "paintbrush", dice = ROT.Dice:new('8d6', 1)}}
	colorOrb = {x = 1, y = 1}
	doTheThing()
end

function mapCallback(x, y, val)
	map[x..','..y] = val
end

function lightCallback(fov, x, y)
	local key = x..','..y
	if map[key] then
		return map[key] == 0
	end
	return false
end

function computeCallback(x, y, r, v)
	local key = x..','..y
	if not map[key] then 
		return 
	end
	field[key] = 1
	seen[key] = 1
end

function placePlayer()
	local key = nil
	local char = '#'
	while true do
		key = ROT.RNG:random(1, f:getWidth())..','..
			ROT.RNG:random(1, f:getHeight())
		if map[key] == 0 then
			pos = key:split(',')
			player.x, player.y = tonumber(pos[1]), tonumber(pos[2])
			f:write('@', player.x, player.y)
			break
		end
	end
end

function placeOrb()
	local key = nil
	local char = '#'
	while true do
		key = ROT.RNG:random(1, f:getWidth())..','..
			ROT.RNG:random(1, f:getHeight())
		if map[key] == 0 then
			map[key] = 2
			pos = key:split(',')
			colorOrb.x, colorOrb.y = tonumber(pos[1]), tonumber(pos[2])
			f:write('B', colorOrb.x, colorOrb.y)
			break
		end
	end
end

function doTheThing()
	uni = ROT.Map.Brogue:new(f:getWidth(), f:getHeight())
	uni:create(mapCallback)
	fov = ROT.FOV.Precise:new(lightCallback)
	placePlayer()
	placeOrb()
	fov:compute(player.x, player.y, 10, computeCallback)
end

function love.update()
	if update then
		update = false
		seen = {}
		doTheThing()
	end
	f:clear()
	for x = 1, f:getWidth() do
		for y = 1, f:getHeight() do
			local key = x..','..y
			if seen[key] then
				char = key == player.x..','..player.y and '@' or map[key] == 0 and '.' or 
					map[key] == 1 and '#' or map[key] == 2 and 'B'
				f:write(char, x, y, field[key] and fieldColor or seenColor, traveled[key] 
					and traveledBg or nil)
			end
		end
	end
	local s = "Use arrowkeys/numpad to move!"
	f:write(s, f:getWidth() - #s, f:getHeight())
end

function love.keypressed(key)
  local newPos={0,0}
  if     key=='kp1' then newPos={-1, 1}
  elseif key=='kp2' or key=='down' then newPos={ 0, 1}
  elseif key=='kp3' then newPos={ 1, 1}
  elseif key=='kp4' or key=='left' then newPos={-1, 0}
  elseif key=='kp5' then newPos={ 0, 0}
  elseif key=='kp6' or key=='right' then newPos={ 1, 0}
  elseif key=='kp7' then newPos={-1,-1}
  elseif key=='kp8' or key=='up' then newPos={ 0,-1}
  elseif key=='kp9' then newPos={ 1,-1}
  --TODO MIGHTFIX delete this and make a menu
  elseif key == 'escape' then love.event.quit(0)
  end
	if newPos ~= {0, 0} then
		local newX = player.x + newPos[1]
		local newY = player.y + newPos[2]
		if map[newX..','..newY] == 0 then
			key = player.x..','..player.y
			traveled[key] = key
			field = {}
			player.x = newX
			player.y = newY
			fov:compute(player.x, player.y, 10, computeCallback)
		elseif map[newX..','..newY] == 2 then
			loadState("win")
--			map[newX..','..newY] = 0
--			key = player.x..','..player.y
--			traveled[key] = key
--			field = {}
--			player.x = newX
--			player.y = newY
--			fov:compute(player.x, player.y, 10, computeCallback)
		end
	end
end

function love.draw()
	f:draw()
end
	